<?php
// Load custom bootstrap for using application's classes
require_once('app/bootstrap.php');
// Load composer's autoloader for all required classes
require_once('vendor/autoload.php');