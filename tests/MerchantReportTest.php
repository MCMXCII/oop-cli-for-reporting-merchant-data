<?php

/**
 * Merchant Report Test
 * --------------------
 * This is the collection of tests that we will run to confirm the merchant report functionality is 
 * working as expected.
 */

class MerchantReportTest extends PHPUNIT_Framework_TestCase
{
	/**
	 * Test Merchant Stores ID
	 * -----------------------
	 * A simple test to confirm the construct is wokring fine and we have access to the merchant class.
	 */
	public function testMerchantStoresId()
	{
		$merchant = new Merchant(2);
		$this->assertEquals(2, $merchant->id);
	}

	/**
	 * Test Currency Agent Can Get Symbol
	 * ----------------------------------
	 * If we were plugging into a 3rd party API, we would want to run continuous tests to confirm the 
	 * API is still running. 
	 * As the construct of the currency agent calls its own symbol API method, we can simply check to 
	 * see if the symbol set against the class is correct.
	 */
	public function testCurrencyAgentCanGetSymbol()
	{
		$currencyAgent = new CurrencyAgent("GBP");
		$this->assertEquals("£", $currencyAgent->symbol);
	}

	/**
	 * Test Currency Agent Rates Are Valid
	 * -----------------------------------
	 * If we were using a 3rd party exchange rate API, we might want to check that firstly, the number 
	 * we are receiving is indeed a float, and secondly, that it is within an expected / realistic 
	 * range, just incase the API is still running, but there is a problem with the data it is outputting.
	 */
	public function testCurrencyAgentRatesAreValid()
	{
		$currencyAgent = new CurrencyAgent("GBP");
		$rate = $currencyAgent->getRate("EUR");
		$this->assertTrue(is_float($rate));
		$this->assertTrue($rate > 0.2 && $rate < 3);
	}

	/**
	 * Test Data Source Is Live
	 * ------------------------
	 * As we are getting data from a possibly external source, we would want to make sure that the 
	 * connection is up and running at all times and that the record sets only contain records.
	 */
	public function testDataSourceIsLive()
	{
		if (DRIVERTYPE == "CSV") {
			$driver = new CSV_Driver();
		} else {
			$driver = new DB_Driver();
		}

		$recordSet = $driver->getData();

		$this->assertTrue(count($recordSet) > 0);
		$this->assertContainsOnly(Record, $recordSet);
	}
}