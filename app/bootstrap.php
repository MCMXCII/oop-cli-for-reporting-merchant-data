<?php

/**
 * Lets get this show on the road.
 * We'll start by booting up the predefined config values from the config file, then we will register 
 * a class finder, so we don't have to `require_once()` every file we wish to include.
 */

require_once('config.php');

spl_autoload_register(function($class){
	require_once(APPPATH . "models/" . $class . ".php");
});

/**
 * As a driver we will be using to read from the data source will be required everywhere else, we will 
 * also be instantiating that here, along with the dataAbstraction class, so it is globally accessible.
 */

$driverType = DRIVERTYPE . "_Driver";
$driverClass = new $driverType();
$dataClass = new dataAbstraction($driverClass);
