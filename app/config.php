<?php

/**
 * This is where we will predefined some constants, like the location of the app, the base url for 
 * future web interfaces, and the driver we wish to use to load the merchant data, along with sections 
 * for specific data relating to that driver.
 */

define('APPPATH', __DIR__ . "/");
define('DRIVERTYPE', "CSV"); // CSV or DB

//-------------
// CSV Settings
//-------------
define('CSVLOCATION', APPPATH . "data/data.csv");
define('CSVDELIMITER', ";");

//------------
// DB Settings
//------------
define('DBDRIVER', "mysql");
define('DBSERVER', "localhost");
define('DBNAME', "testdb");
define('DBUSER', "username");
define('DBPASS', "password");
define('DBTABLE', "records");