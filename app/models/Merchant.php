<?php

/**
 * Merchant
 * --------
 * The merchant class is a object representation of the merchant ID, in a real world 
 * example this would have multiple properties and various functions, however for 
 * the scope of this application, it requires only an ID and an arra of records 
 * associaited to it.
 */

class Merchant extends RecordableObject
{
	public $id;

	public function __construct($id)
	{
		$this->id = $id;
	}

	/**
	 * Convert Records
	 * ---------------
	 * Using a provided currency agent, update the value of any foreign records into the provided 
	 * currency, as stipulated in the instantiation of the currency agent.
	 * 
	 * @param  CurrencyAgent $currency A currency agent, used for gathering up to date currency information.
	 */
	public function convertRecords(CurrencyAgent $currency)
	{
		foreach ($this->records as &$record) {			
			$currencySymbol = preg_replace('/[0-9,.]+/', '', $record->value);
			$currencyCode = $currency->getCurrencyCode($currencySymbol);
			$amount = str_replace($currencySymbol, "", $record->value);
			$rate = $currency->getRate($currencyCode);
			$record->value = $currency->symbol . number_format($amount * $rate, 2);
		}
	}
}