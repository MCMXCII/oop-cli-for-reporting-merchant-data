<?php

/**
 * View
 * ----
 * A static class responsible for renderering a view and making data accessible within that view.
 */

class View
{
	/**
	 * View
	 * ----
	 * Render the given filename, makign the given data accessible.
	 * 
	 * @param  String $filename The name / path of the view we wish to render.
	 * @param  Mixed $data      The data we want accessible in the view.
	 */
	public static function render($filename, $data) 
	{
		$filepath = APPPATH . "views/" . $filename . ".phtml";
		if (!file_exists($filepath)) {
			throw new CLIReply('The following report is missing: ' . $filename . '.');
		}
		include $filepath;
	}
}