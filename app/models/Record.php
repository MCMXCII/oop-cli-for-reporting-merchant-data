<?php

/**
 * Record
 * --------
 * The record class is an object representation of each line retrieve form the data 
 * source. It will have properties set against it that will be programmatically 
 * set by the data abstraction.
 */

class Record
{
	public $merchant;
	public $date;
	public $value;
}