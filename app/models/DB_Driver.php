<?php

/**
 * DB Driver
 * ----------
 * The DB Driver, which extends from the driverClass, will provide the functionality for accessing data 
 * in a table based on the standardised functions, extended from the driverClass.
 */

class DB_Driver extends driverClass implements driverInterface
{
	private $connection;

	public function __construct()
	{
		$this->connection = new PDO(DBDRIVER. ':host='.DBSERVER. ';dbname='.DBNAME,DBUSER, DBPASS);
	}

	private function _getTableData()
	{
		$query = $this->connection->query("SELECT * FROM " . DBTABLE);
		$results = $query->fetchAll();
		return $results;
	}

	public function getData()
	{
		$data = $this->_getTableData();
		return $this->prepareRecords($data);
	}
}