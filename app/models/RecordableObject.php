<?php

/**
 * Recordable Object
 * -----------------
 * The recordable object class is the parent for any class that can hold records. In the scope of this 
 * application that is merchants, however in a real world example, it could be a number of classes.
 */

class RecordableObject
{
	public $records = [];
}