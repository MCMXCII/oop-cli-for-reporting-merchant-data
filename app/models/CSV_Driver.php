<?php

/**
 * CSV Driver
 * ----------
 * The CSV Driver, which extends from the driverClass, will provide the functionality for parsing CSV 
 * files based on the standardised functions, extended from the driverClass.
 */

class CSV_Driver extends driverClass implements driverInterface
{
	private $fileData;

	/**
	 * Get File Data
	 * -------------
	 * Get the CSV data out of the data source, append the first line as headers and set it against 
	 * the object for future retrievals.
	 * 
	 * @return Array An array containing all of the rows of the CSV file.
	 */
	private function _getFileData()
	{
		if (empty($this->fileData)) {
			if (!file_exists(CSVLOCATION)) {
				throw new Exception('The following .csv file doesn\'t exist: ' . CSVLOCATION . '.');
			}
			if (!is_readable(CSVLOCATION)) {
				throw new Exception('Unable to read from .csv file ' . CSVLOCATION . '.');
			}
			if (pathinfo(CSVLOCATION)['extension'] != 'csv') {
				throw new Exception(CSVLOCATION . ' is not a valid .csv file');
			}
			$handle = fopen(CSVLOCATION, "r");
			$row = 1;
			$headers = fgetcsv($handle, 0, CSVDELIMITER);
			if (count($headers) <= 1) {
				throw new Exception('Couldn\'t retrieve valid data from the provided .csv file, please check the delimiter.');
			}
			while (!feof($handle)) {
				$rowData = fgetcsv($handle, 0, CSVDELIMITER);
				foreach ($headers as $key => $value) {
					$fileDataRow[$value] = $rowData[$key];
				}
				$fileData[] = $fileDataRow;
				$row++;
			}
			$this->fileData = $fileData;
		}
		return $this->fileData;
	}

	/**
	 * Get Data
	 * --------
	 * Get all the data out of the CSV file and return it.
	 * 
	 * @return Array An array containing all of the rows of the CSV file.
	 */
	public function getData()
	{
		$data = $this->_getFileData();
		return $this->prepareRecords($data);
	}
}