<?php

/**
 * Report
 * ------
 * The report class is an object representation of a report. Any objects / report types passed to it 
 * will exist as a report in the view directory.
 */

class Report
{
	private $reportName;
	private $object;

	/**
	 * We need to generate the filename of the view that relates to this report object and store the 
	 * objects data for later use.
	 */
	public function __construct($object, $reportType) 
	{
		$this->reportName = strtolower(get_class($object)) . "/" . $reportType;
		$this->object = $object;
	}

	/**
	 * Render
	 * ------
	 * Using the static method on the view model, output the contents of the report file, passing through 
	 * the object's data.
	 */
	public function render()
	{
		View::render('reports/' . $this->reportName, $this->object);
	}
}