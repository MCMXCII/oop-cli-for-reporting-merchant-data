<?php

/**
 * CLI Reply
 * ---------
 * The CLI reply class allows us to throw an exception to the stdOut that isn't specifically an error.
 */

class CLIReply extends Exception
{
}