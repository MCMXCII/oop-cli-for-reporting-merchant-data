<?php

/**
 * Data Abstraction
 * ----------------
 * The data abstraction allows for complete encapsulation when it comes to reading from the data source.
 * Higher up classes will call the data abstraction to retrieve the data, regardless of the driver type.
 */

class dataAbstraction
{
	private $driver;

	/**
	 * Dependency inject the driver we are going to use when setting up the dataAbstraction.
	 */
	public function __construct(driverClass $driver)
	{
		$this->driver = $driver;
	}

	/**
	 * Get Data
	 * --------
	 * Get all the data from the data source matching the given objects key.
	 * 
	 * @param  RecordableObject $object     The object in which to set the data onto.
	 * @param  String           $objectKey  The key on the object in which to search for.
	 * @param  String           $dataKey    The key of the data in which to search for.
	 * @return RecordableObject         The object once the data has bene set on it.
	 */
	public function getData(RecordableObject $object, $objectKey, $dataKey)
	{
		$object->records = $this->driver->getRecordsWhere($dataKey, $object->$objectKey);
		if (empty($object->records)) {
			throw new CLIReply('Can\'t find any records with that ' . strtolower(get_class($object)) . '\'s ' . $objectKey . '.');
		}
		return $object;
	}
}