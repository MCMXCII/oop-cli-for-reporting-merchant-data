<?php

/**
 * Driver Class
 * ------------
 * The driver class will provide standardised functions for the different driver types, which will 
 * therefore be accessible for higher up classes to access.
 */

class driverClass
{
	/**
	 * Get Records Where
	 * -----------------
	 * Get all records matching a given key / value pair criteria.
	 * 
	 * @param  String $key   The key in which to search the records by.
	 * @param  String $value The value in which to search for.
	 * @return Array         An array of records matching the given criteria.
	 */
	public function getRecordsWhere($key, $value)
	{
		$records = $this->getData();
		return array_filter($records, function($record) use ($key, $value){
			return $record->$key == $value;
		});
	}

	/**
	 * Get All Records
	 * ---------------
	 * Get all the records.
	 * 
	 * @return Array         An array of all the records.
	 */
	public function getAllRecords()
	{
		return $this->getData();
	}

	protected function prepareRecords($data)
	{
		$recordSet = [];
		foreach ($data as $fileRow) {
			$record = new Record();
			foreach ($fileRow as $key => $value) {
				if (property_exists($record, $key)) {					
					$record->$key = $value;
				}
			}
			$recordSet[] = $record;
		}
		return $recordSet;
	}

}