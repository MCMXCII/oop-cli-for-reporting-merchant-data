<?php

/**
 * Currency Agent
 * --------------
 * The currency agent class handles pulling in currency codes and symbols to be converted and pulling 
 * in up to date exchange rates, everything that will be needed for converting currency.
 */

class CurrencyAgent
{
	public $symbol;
	public $code;
	private $currencyCodes;
	private $currencySymbols;
	private $currencyRates;

	/**
	 * Set the provided base currency against the class for future use.
	 */
	public function __construct($code)
	{
		$this->code = $code;
		$this->symbol = $this->getCurrencySymbol($code);

	}

	/**
	 * Get Currency Symbol
	 * -------------------
	 * In a real world system, this would call a 3rd party API and pull in all the currency symbols, 
	 * indexed by their currency codes.
	 * 
	 * @param  String $code The currency code we need the symbol for.
	 * @return String       The symbol of the currency code.
	 */
	public function getCurrencySymbol($code)
	{
		// No sense in calling a 3rd party API everytime
		if (empty($this->currencySymbols)) {
			# Call a 3rd party API and get the symbols.
			# if (!API->return) {
			# 	throw new Exception('Unable to connect to currency symbol API.');
			# }
			$this->currencySymbols = [
				'GBP' => '£',
				'USD' => '$',
				'EUR' => '€'
			];
		}
		
		if (empty($this->currencySymbols[$code])) {
			throw new CLIReply('We haven\'t been provided a symbol for the currency code: ' . $code . '.');
		}
		return $this->currencySymbols[$code];
	}

	/**
	 * Get Currency Code
	 * -----------------
	 * In a real world system, this would call a 3rd party API and pull in all the currency codes, 
	 * indexed by their currency symbols.
	 * 
	 * @param  String $symbol The currency symbol we need the code for.
	 * @return String         The code of the currency symbol.
	 */
	public function getCurrencyCode($symbol)
	{

		// No sense in calling a 3rd party API everytime
		if (empty($this->currencyCodes)) {
			# Call a 3rd party API and get the codes.
			# if (!API->return) {
			# 	throw new Exception('Unable to connect to currency code API.');
			# }
			$this->currencyCodes = [
				"£" => 'GBP',
				"$" => 'USD',
				"€" => 'EUR'
			];
		}
		if (empty($this->currencyCodes[$symbol])) {
			throw new CLIReply('We haven\'t been provided a code for the currency symbol: ' . $symbol . '.');
		}
		return $this->currencyCodes[$symbol];
	}

	/**
	 * Get Rate
	 * --------
	 * In a real world system, this would call a 3rd party API and pull in up to date exchange rates.
	 *  
	 * @param  String $code The currency code we need the exchange rate from.
	 * @return Int          The exchange rates from the given currency.
	 */
	public function getRate($code)
	{
		// No sense in calling a 3rd party API everytime
		if (empty($this->currencyRates)) {
			# Call a 3rd party API and get the rates.
			# if (!API->return) {
			# 	throw new Exception('Unable to connect to currency rates API.');
			# }
			$this->currencyRates = [
				'GBP' => [
					'EUR' => 0.88,
					'USD' => 0.72
				]
			];
		}
		if (empty($this->currencyRates[$this->code][$code]) && $this->code != $code) {
			throw new CLIReply('We haven\'t been provided an rate for the following currency exchange: ' . $code . " to " . $this->code);
		}
		return $this->code == $code ? 1 : $this->currencyRates[$this->code][$code];
	}
}