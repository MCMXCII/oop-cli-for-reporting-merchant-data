<?php

/**
 * Driver Interface
 * ----------------
 * The driver interface will blueprint the bare minimal functions required for a driver.
 */

interface driverInterface
{
	public function getData();
}