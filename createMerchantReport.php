<?php

/**
 * CLI function for producing a report of the transactions of merchants, for a given ID, 
 * arrange by the date.
 */

require_once('app/bootstrap.php');

try {
	if (empty($argv[1])) {
		throw new CLIReply('Please provide a Merchant ID');
	}
	
	// Instantiate a merchant object and populate it with the relevant data
	$merchant = new Merchant($argv[1]);
	$merchant = $dataClass->getData($merchant, 'id', 'merchant');

	// Instantiate a currency agent and inject it into the merchant's conversion method to convert 
	// any foreign records the merchant may have into GBP
	$englishCurrencyAgent = new CurrencyAgent('GBP');
	$merchant->convertRecords($englishCurrencyAgent);

	// Generate a report and then report it to the stdOut
	$report = new Report($merchant, 'transactions');
	$report->render();	
} catch (CLIReply $e) {
	echo $e->getMessage();
} catch (Exception $e) {
	echo 'Error: ' . $e->getMessage();
}